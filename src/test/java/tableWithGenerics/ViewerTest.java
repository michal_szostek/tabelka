package tableWithGenerics;

import junit.framework.TestCase;
import org.junit.Assert;
import org.junit.Test;
import tableWithGenerics.Types.Message;

public class ViewerTest extends TestCase {

    Message msg = new Message(1, "11.12.20", "Doe", "Lorem ipsum");
    Viewer<Message> viewer = new Viewer<>(msg);

    @Test
    public void testCallAction() {
        viewer.callAction();
        Assert.assertEquals(true, msg.getRead());
    }

    @Test
    public void testClose() {
        viewer.close();
    }
}