package tableWithGenerics;

import junit.framework.TestCase;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import tableWithGenerics.Types.Author;

public class TableTest extends TestCase {

    Table<Author> authors = new Table<>();

    @Before
    public void setUp() {
        authors.add(new Author(1, "John", "Doe"));
        authors.add(new Author(2, "J.R.R", "Tolkien"));
        authors.add(new Author(3, "Jan", "Nowak"));
    }

    @Test
    public void testAdd() {
        Table<Author> authors = new Table<Author>();
        authors.add(new Author(1, "John", "Doe"));
        Assert.assertEquals(1, authors.getNumberOfElements());
    }

}