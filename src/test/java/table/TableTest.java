package table;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TableTest {

    private final Table table = new Table(0, 2, 1);

    @Before
    public void setUp() {
        //region Init articleList
        List<Article> articleList = new ArrayList<>();
        articleList.add(
                new Article(1, "Using Arrays",
                        new Author(1, "John", "Doe"),
                        "One final use of reflection is in creating and manipulating arrays. Arrays in the Java language are a specialized type of class, and an array reference can be assigned to an Object reference. To see how arrays work, consider the following example:",
                        new Date()));
        articleList.add(
                new Article(2, "Using Arrays cd",
                        new Author(1, "John", "Doe"),
                        "One final use of reflection is in creating and manipulating arrays. Arrays in the Java language are a specialized type of class, and an array reference can be assigned to an Object reference. To see how arrays work, consider the following example:",
                        new Date()));
        articleList.add(
                new Article(3, "Using Arrays cd",
                        new Author(1, "John", "Doe"),
                        "One final use of reflection is in creating and manipulating arrays. Arrays in the Java language are a specialized type of class, and an array reference can be assigned to an Object reference. To see how arrays work, consider the following example:",
                        new Date()));
        articleList.add(
                new Article(4, "Using Arrays cd",
                        new Author(1, "John", "Doe"),
                        "One final use of reflection is in creating and manipulating arrays. Arrays in the Java language are a specialized type of class, and an array reference can be assigned to an Object reference. To see how arrays work, consider the following example:",
                        new Date()));
        articleList.add(
                new Article(5, "Using Arrays cd",
                        new Author(1, "John", "Doe"),
                        "One final use of reflection is in creating and manipulating arrays. Arrays in the Java language are a specialized type of class, and an array reference can be assigned to an Object reference. To see how arrays work, consider the following example:",
                        new Date()));
        articleList.add(
                new Article(6, "Using Arrays cd",
                        new Author(1, "John", "Doe"),
                        "One final use of reflection is in creating and manipulating arrays. Arrays in the Java language are a specialized type of class, and an array reference can be assigned to an Object reference. To see how arrays work, consider the following example:",
                        new Date()));
        articleList.add(
                new Article(7, "Using Arrays cd",
                        new Author(1, "John", "Doe"),
                        "One final use of reflection is in creating and manipulating arrays. Arrays in the Java language are a specialized type of class, and an array reference can be assigned to an Object reference. To see how arrays work, consider the following example:",
                        new Date()));
        articleList.add(
                new Article(8, "Using Arrays cd",
                        new Author(1, "John", "Doe"),
                        "One final use of reflection is in creating and manipulating arrays. Arrays in the Java language are a specialized type of class, and an array reference can be assigned to an Object reference. To see how arrays work, consider the following example:",
                        new Date()));
        //endregion
        table.setArticleList(articleList);
        table.addArticle(new Article(9, "Using Arrays",
                new Author(1, "John", "Doe"),
                "One final use of reflection is in creating and manipulating arrays. Arrays in the Java language are a specialized type of class, and an array reference can be assigned to an Object reference. To see how arrays work, consider the following example:",
                new Date()));
    }

    @Test
    public void testNumberOfArticles() {
        Assert.assertEquals(table.getNumberOfArticles(), table.showArticles().size());
    }

    @Test
    public void testChangePage() {
        table.changePage(3);
        Assert.assertEquals(3, table.getCurrentPage());
    }

    @Test
    public void testNextPage() {
        table.changePage(0);
        table.nextPage();
        table.nextPage();
        Assert.assertEquals(2, table.getCurrentPage());
    }

    @Test
    public void testPrevPage() {
        table.changePage(3);
        table.prevPage();
        table.prevPage();
        Assert.assertEquals(1, table.getCurrentPage());
    }

    @Test
    public void testPrevPageIfIsSmallerThan0() {
        table.changePage(1);
        table.prevPage();
        table.prevPage();
        table.prevPage();
        table.prevPage();
        table.prevPage();
        Assert.assertEquals(0, table.getCurrentPage());
    }

    @Test
    public void testShowArticle() {
        table.changePage(0);
        List<Article> articleList1 = table.showArticles();
        Article article = table.showArticle(articleList1.get(0));
        Assert.assertEquals(1, article.getId());
    }
}