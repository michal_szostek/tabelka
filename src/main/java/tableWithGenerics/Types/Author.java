package tableWithGenerics.Types;

import tableWithGenerics.Tableable;
import java.util.Arrays;
import java.util.List;

public class Author implements Tableable {

    private final int id;
    private final String firstName;
    private final String lastName;
    private static final List<String> COLUMNS = Arrays.asList("id", "first name", "last name");
    private List<String> values = null;

    public Author(int id, String firstName, String lastName) {
        super();
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public int getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }


    @Override
    public List<String> getColumns() {
        return COLUMNS;
    }

    @Override
    public List<String> getValues() {
        return Arrays.asList(
                Integer.toString(this.id),
                this.getFirstName(),
                this.lastName
        );
    }

    @Override
    public String toString() {
        return this.firstName + " " + this.lastName;
    }
}