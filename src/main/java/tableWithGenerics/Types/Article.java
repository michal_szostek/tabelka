package tableWithGenerics.Types;

import tableWithGenerics.Tableable;
import tableWithGenerics.Viewable;

import java.util.Arrays;
import java.util.List;

public class Article implements Tableable, Viewable {

    private int id;
    private String title;
    private Author author;
    private String content;
    private String published;
    private Boolean watched;
    private static final List<String> COLUMNS = Arrays.asList("id", "title", "author", "content", "published", "watched");
    private List<String> values = null;


    public Article(int id, String title, Author author, String content, String published) {
        this.id = id;
        this.title = title;
        this.author = author;
        this.content = content;
        this.published = published;
        this.watched = false;
    }

    public Article showArticle() {
        return this;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public Author getAuthor() {
        return author;
    }

    public String getContent() {
        return content;
    }

    @Override
    public void close() {
        System.out.println("Modal closed.");
    }

    @Override
    public void callAction() {
        this.watched = true;
        System.out.println("Article " + this.title + " watched.");
    }

    @Override
    public List<String> getColumns() {
        return COLUMNS;
    }

    @Override
    public List<String> getValues() {
        return Arrays.asList(
                Integer.toString(this.id),
                this.title,
                this.author.toString(),
                this.content,
                this.published
        );
    }
}
