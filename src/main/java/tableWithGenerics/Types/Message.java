package tableWithGenerics.Types;

import tableWithGenerics.Tableable;
import tableWithGenerics.Viewable;

import java.util.Arrays;
import java.util.List;

public class Message implements Tableable, Viewable {

    private int id;
    private String date;
    // docelowo User
    private String from;
    private String content;
    private Boolean read = false;
    private static final List<String> COLUMNS = Arrays.asList("id", "date", "from", "content");
    private List<String> values = null;


    public Message(int id, String date, String from, String content) {
        this.id = id;
        this.date = date;
        this.from = from;
        this.content = content;
    }

    @Override
    public List<String> getColumns() {
        return COLUMNS;
    }

    @Override
    public List<String> getValues() {
        return Arrays.asList(
                Integer.toString(this.id),
                this.date,
                this.from,
                this.content
        );

    }

    @Override
    public String getTitle() {
        return "Message: " + this.id;
    }

    @Override
    public Message getContent() {
        return this;
    }

    @Override
    public void close() {
        System.out.println("Modal closed.");
    }

    @Override
    public void callAction() {
        this.read = true;
        System.out.println("Message read.");
    }

    public Boolean getRead() {
        return read;
    }
}
