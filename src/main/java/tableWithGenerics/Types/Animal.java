package tableWithGenerics.Types;

import tableWithGenerics.Tableable;

import java.util.List;

public class Animal implements Tableable {

    private int id;
    private String name;
    private String description;
    //docelowo enum
    private String species;

    public Animal(int id, String name, String description, String species) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.species = species;
    }


    @Override
    public List<String> getColumns() {
        return null;
    }

    @Override
    public List<String> getValues() {
        return null;
    }
}
