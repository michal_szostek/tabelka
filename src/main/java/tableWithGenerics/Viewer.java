package tableWithGenerics;

public class Viewer <T extends Viewable> {

    private T element;

    public Viewer(T element) {
        this.element = element;
    }

    void close() {
        element.close();
    }

    void callAction() {
        element.callAction();
    }

}
