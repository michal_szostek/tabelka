package tableWithGenerics;

import tableWithGenerics.Types.Article;
import tableWithGenerics.Types.Author;
import tableWithGenerics.Types.Message;


public class Main {

    public static void main(String[] args) {

        Table<Author> authors = new Table<>();
        Author author = new Author(1, "John", "Doe");
//        authors.add(new Author(2, "J.R.R", "Tolkien"));
//        authors.add(new Author(3, "Jan", "Nowak"));

//        Table<Message> msg = new Table<>();
//        msg.add(new Message(1, "11.12.20", "Doe", "Lorem ipsum"));
//        msg.add(new Message(2, "12.12.20", "John", "Lorem ipsum"));
//        msg.printTable();
//        msg.openDetails(1);
//        msg.callItemAction(1);
//
//        Viewer<Message> message = new Viewer<>(msg.get(1));
//        message.callAction();
//        message.close();

        Table<Article> article = new Table<>();
        article.add(new Article(1,"a", author, "aas", "11.12.21"));
        article.add(new Article(2,"b", author, "aas", "10.11.21"));

        article.printTable();

        article.openDetails(0);
        article.callItemAction(0);
        Viewer<Article> articleViewer = new Viewer<>(article.get(0));
        articleViewer.callAction();
        articleViewer.close();


    }

}
