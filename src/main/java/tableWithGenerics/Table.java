package tableWithGenerics;

import java.util.ArrayList;
import java.util.List;

public class Table<T extends Tableable> {

    private String title;
    private int numberOfFirstElementOnPage;
    private int elementsPerPage;
    private int currentPage;
    private List<T> elements;
    private List<String> headers;

    public Table() {
        this.elements = new ArrayList<>();
    }

    public void add(T element) {
        elements.add(element);
    }

    public int getNumberOfElements() {
        return elements.size();
    }

    public T get(int index) {
        return elements.get(index);
    }

    public void printTable() {

        List<T> elementsToShow = new ArrayList<>();

        System.out.println(this.elements.get(0).getClass());

        System.out.println(this.elements.get(0).getColumns());

        for (T t : this.elements) {
            System.out.println(t.getValues());
        }
    }

    public void callItemAction(int index) {

    }

    // TODO @michaszo - 2020-09-29 wt. 10:34 - Ujednolicić z wywyołaniem Viewiera
    public void openDetails(int index) {
        List<String> element = this.elements.get(index).getValues();

        System.out.println("You are in element " + element);
        System.out.println("2. Call method");
        System.out.println("3. exit()");
    }
}
