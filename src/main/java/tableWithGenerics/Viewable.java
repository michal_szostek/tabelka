package tableWithGenerics;

public interface Viewable<T> {

    String getTitle();

    T getContent();

    void close();

    void callAction();

}
