package tableWithGenerics;

import java.util.List;

public interface Tableable {

    List<String> getColumns();

    List<String> getValues();

}
