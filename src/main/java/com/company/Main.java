package com.company;

import table.Article;
import table.Author;
import table.Table;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        Table table = new Table(0, 3, 1);
        //region Init articleList
        List<Article> articleList = new ArrayList<>();
        articleList.add(
                new Article(1, "Using Arrays",
                        new Author(1, "John", "Doe"),
                        "One final use of reflection is in creating and manipulating arrays. Arrays in the Java language are a specialized type of class, and an array reference can be assigned to an Object reference. To see how arrays work, consider the following example:",
                        new Date()));
        articleList.add(
                new Article(2, "Using Arrays cd",
                        new Author(1, "John", "Doe"),
                        "One final use of reflection is in creating and manipulating arrays. Arrays in the Java language are a specialized type of class, and an array reference can be assigned to an Object reference. To see how arrays work, consider the following example:",
                        new Date()));
        articleList.add(
                new Article(3, "Using Arrays cd",
                        new Author(1, "John", "Doe"),
                        "One final use of reflection is in creating and manipulating arrays. Arrays in the Java language are a specialized type of class, and an array reference can be assigned to an Object reference. To see how arrays work, consider the following example:",
                        new Date()));
        articleList.add(
                new Article(4, "Using Arrays cd",
                        new Author(1, "John", "Doe"),
                        "One final use of reflection is in creating and manipulating arrays. Arrays in the Java language are a specialized type of class, and an array reference can be assigned to an Object reference. To see how arrays work, consider the following example:",
                        new Date()));
        articleList.add(
                new Article(5, "Using Arrays cd",
                        new Author(1, "John", "Doe"),
                        "One final use of reflection is in creating and manipulating arrays. Arrays in the Java language are a specialized type of class, and an array reference can be assigned to an Object reference. To see how arrays work, consider the following example:",
                        new Date()));
        articleList.add(
                new Article(6, "Using Arrays cd",
                        new Author(1, "John", "Doe"),
                        "One final use of reflection is in creating and manipulating arrays. Arrays in the Java language are a specialized type of class, and an array reference can be assigned to an Object reference. To see how arrays work, consider the following example:",
                        new Date()));
        articleList.add(
                new Article(7, "Using Arrays cd",
                        new Author(1, "John", "Doe"),
                        "One final use of reflection is in creating and manipulating arrays. Arrays in the Java language are a specialized type of class, and an array reference can be assigned to an Object reference. To see how arrays work, consider the following example:",
                        new Date()));
        articleList.add(
                new Article(8, "Using Arrays cd",
                        new Author(1, "John", "Doe"),
                        "One final use of reflection is in creating and manipulating arrays. Arrays in the Java language are a specialized type of class, and an array reference can be assigned to an Object reference. To see how arrays work, consider the following example:",
                        new Date()));
        table.setArticleList(articleList);
        table.addArticle(new Article(9, "Using Arrays",
                new Author(1, "John", "Doe"),
                "One final use of reflection is in creating and manipulating arrays. Arrays in the Java language are a specialized type of class, and an array reference can be assigned to an Object reference. To see how arrays work, consider the following example:",
                new Date()));
        //endregion

//        table.watched(articleList.get(1));

        System.out.println("Current page: " + table.getCurrentPage());
        table.nextPage();
        System.out.println("Current page: " + table.getCurrentPage());
        table.nextPage();
        System.out.println("Current page: " + table.getCurrentPage());
//        table.nextPage();
//        System.out.println("Current page: " + table.getCurrentPage());
//        table.prevPage();
//        System.out.println("Current page: " + table.getCurrentPage());
//        table.prevPage();
//        System.out.println("Current page: " + table.getCurrentPage());

//        System.out.println(table.getNumberOfArticles());

//        table.changePage(3);

        for (Article article : table.showArticles()) {
            if (article.getWatched()) {
                System.out.println(article.getId() + " - watched");
            } else {
                System.out.println(article.getId());
            }
        }

//        Article articleToShow = table.showArticle(articleList.get(1));
//        System.out.println(articleToShow.getId() + ". " + articleToShow.getTitle());

        table.changePage(0);
        List<Article> articleList1 = table.showArticles();
        Article article = table.showArticle(articleList1.get(0));
        System.out.println(article.getId());
//        assertEquals(1, article.getId());


        //co tabela powinna wiedzieć od obiektu
        //delegaty dla kliknięcia
    }
}
