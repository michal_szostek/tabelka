package table;

import java.util.ArrayList;
import java.util.List;

public class Table {

    private int numberOfFirstArticleOnPage;
    private int articlesPerPage;
    private int currentPage;
    private List<Article> articleList = new ArrayList<>();
    private List<Article> headers = new ArrayList<>();
    private String title;

    public Table(int numberOfFirstArticleOnPage, int articlesPerPage, int currentPage) {
        this.numberOfFirstArticleOnPage = numberOfFirstArticleOnPage;
        this.articlesPerPage = articlesPerPage;
        this.currentPage = currentPage;
    }

    public Article showArticle(Article article) {
        return article;
    }

    public void addArticle(Article article) {
        this.articleList.add(article);
    }

    public void nextPage() {
        this.currentPage++;
        this.numberOfFirstArticleOnPage += this.articlesPerPage;
        showArticles();
    }

    public void prevPage() {
        if (currentPage > 0) {
            this.currentPage--;
            this.numberOfFirstArticleOnPage -= this.articlesPerPage;
        }
        showArticles();
    }

    public void setArticleList(List<Article> articleList) {
        this.articleList = articleList;
    }

    public List<Article> showArticles() {

        List<Article> articleListToShow = new ArrayList<>();

        for (int i = this.numberOfFirstArticleOnPage; i < this.articleList.size(); i++) {

            articleListToShow.add(this.articleList.get(i));

            if (articleListToShow.size() == articlesPerPage) {
                break;
            }
        }

//        articleListToShow = this.articleList.subList(this.numberOfFirstArticleOnPage, this.numberOfFirstArticleOnPage + this.articlesPerPage);

        return articleListToShow;
    }

    public void changePage(int pageNumber) {
        this.currentPage = pageNumber;
        this.numberOfFirstArticleOnPage = this.articlesPerPage * this.currentPage;
    }


    public void watched(Article article) {
        article.setWatched(true);
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public List<Article> getHeaders() {
        return headers;
    }

    public void setHeaders(List<Article> headers) {
        this.headers = headers;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getArticlesPerPage() {
        return articlesPerPage;
    }

    public void setArticlesPerPage(int articlesPerPage) {
        this.articlesPerPage = articlesPerPage;
    }

    public int getNumberOfArticles() {
        return articleList.size();
    }
}
