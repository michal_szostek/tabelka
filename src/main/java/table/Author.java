package table;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Author {

    private int id;
    private String firstName;
    private String lastName;
    private List<String> columns = new ArrayList<>();
    private List<String> values = null;

    public Author(int id, String firstName, String lastName) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public int getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

}
