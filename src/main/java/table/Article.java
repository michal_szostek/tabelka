package table;

import java.util.Date;

public class Article {

    private int id;
    private String title;
    private Author author;
    private String content;
    private Date published;
    private Boolean watched;


    public Article(int id, String title, Author author, String content, Date published) {
        this.id = id;
        this.title = title;
        this.author = author;
        this.content = content;
        this.published = published;
        this.watched = false;
    }

    public Article showArticle() {
        return this;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public Author getAuthor() {
        return author;
    }

    public String getContent() {
        return content;
    }

    public Date getPublished() {
        return published;
    }

    public Boolean getWatched() {
        return watched;
    }

    public void setWatched(Boolean watched) {
        this.watched = watched;
    }
}
